const config = require('./config.json')

const { parentPort } = require('worker_threads')
parentPort.postMessage(true)

const { default: axios } = require('axios')

setInterval(() => {
    axios.post(`https://api.revolt.chat/channels/${config.target}/messages`, {
        content: config.content,
        nonce: String(Math.random())
    }, {
        headers: {
            'x-user-id': config.uid,
            'x-session-token': config.token
        }
    })
}, config.interval)