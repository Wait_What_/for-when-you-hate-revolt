const { Worker } = require('worker_threads')
const config = require('./config.json')

Promise.all(
    Array(config.threads).fill()
        .map(() => new Promise(resolve =>
            new Worker('./thread.js').on('message', resolve)
        ))
    )
.then(() => console.log('all threads started'))
